import os
import sys
import re
from model.deck import Deck
from model.card import Card
from db.deck import DeckDB


class DeckService:
    def __init__(self, app):
        self.app = app
        self.base_dir = self.app.config['DECK_DATA_FOLDER']

        self.init_new_deck_db('default')
        self.decks = self.lookup_decks()

    def get_all_decks(self):
        self.lookup_decks()
        return self.decks

    def get_deck(self, name):
        self.lookup_decks()
        return self.decks[name]

    def lookup_decks(self):
        decks = {}
        for folder in os.listdir(self.base_dir):
            deck_path = os.path.join(self.base_dir, folder)
            if os.path.isdir(deck_path):
                # connect to DB
                db_file = os.path.join(deck_path, self.app.config['DECK_DB_FILE_NAME'])
                if os.path.isfile(db_file):
                    try:
                        db = DeckDB(db_file)
                        # collect values from DB or use default
                        deck = Deck(db.get_name())
                        deck.db = db
                        deck.folder = folder
                        deck.default_card_back = db.get_default_card_back()
                        deck.cards = db.get_cards()
                        deck.images = []
                        supported_images_regexp = re.compile(db.get_supported_images_regex())
                        for root, dirs, files in os.walk(os.path.join(deck_path, db.get_images_folder())):
                            for file in files:
                                if supported_images_regexp.match(file):
                                    deck.images.append(file)

                        #TODO: remove block below. Debug purpose only
                        deck.cards = []
                        for image in deck.images:
                            if image == 'back.svg':
                                deck.default_card_back = 'back.svg'
                            else:
                                deck.cards.append(Card(image[:3], ['group1'], None, image, None, None))

                        decks[deck.name] = deck
                    except:
                        print('DB file "{0}" read error: {1}.'.format(db_file, sys.exc_info()[0]))
                        continue
                else:
                    print('DB file "{0}" not found. Deck skipped.'.format(db_file))
                    continue
        return decks

    def init_new_deck_db(self, deck_folder):
        try:
            # check if the deck folder already exist
            deck_path = os.path.join(self.base_dir, deck_folder)
            if not os.path.exists(deck_path):
                os.mkdir(deck_path)
            db_file = os.path.join(deck_path, self.app.config['DECK_DB_FILE_NAME'])
            if os.path.exists(db_file):
                os.remove(db_file)
            db = DeckDB(db_file)
            db.set_name(deck_folder)
            db.set_images_folder(self.app.config['DECK_IMAGES_FOLDER'])
            db.set_supported_images_regex(self.app.config['DECK_SUPPORTED_IMAGES_REGEX'])
            return db
        except:
            print('init_new_deck_db failed. Error: ', sys.exc_info()[0])
            return None

    def get_image_absolute_path(self, deck_name, image_file_name):
        deck = self.decks[deck_name]
        return os.path.join(self.base_dir, deck.folder, deck.db.get_images_folder(), image_file_name)
