from model.user import User, AnonymousUser
from app.login_manager import login_user, logout_user
import hashlib
from random import random
from datetime import datetime


class UserService:
    """
    This class provides an interface to get with user login/registration features.
    Currently, it stores password in secure database like a salted SHA1 hashes.
    It should protect passwords against a rainbow tables attack, but probably isn't secure enough.

    TODO: need to investigate a Flask-Bcrypt extension (http://flask-bcrypt.readthedocs.org/en/latest/)
    if it is more efficient and secure.
    """
    def __init__(self, app):
        self.app = app

    def get_user(self, user_id):
        if user_id in self.app.db.users:
            user_db_obj = self.app.db.users[user_id]
            user_obj = UserService._db_obj_to_user(user_db_obj)
            return user_obj
        return None

    def find_user_by_login(self, user_login):
        """ Look for user with specified user_login in DB and return its system user_id value
        :param user_login: String human-readable identifier of user
        :return: system user_id or None if user not found
        """

        user = self.app.db.users[user_login]
        return user['login']

    def find_user_by_ip_address(self, ip_address):
        users = self.app.db.users.values()
        for user in users:
            if user['ip_address'] == ip_address:
                return UserService._db_obj_to_user(user)
        return None

    def is_registered_login(self, user_login):
        return self.app.db.users[user_login] is not None

    def do_login(self, user_login, remember_me):
        self.app.logger.info("do_login")
        login_user(self.get_user(user_login), remember_me)

    def do_logout(self):
        self.app.logger.info("do_logout")
        logout_user()

    def match_password(self, user_id, password):
        password_db = self.app.db.passwords[user_id]
        if password_db is None:
            return True
        password_hash = hashlib.sha1(bytearray(password + password_db['salt'], 'utf-8')).hexdigest()
        return password_hash == password_db['password']

    def register_user(self, user, password=None):
        user_id = user.login
        self.app.db.users[user_id] = {'login': user.login, 'name': user.name, 'roles': user.roles,
                                      'ip_address': user.last_visited_ip, 'is_anonymous': user.is_anonymous,
                                      'registration_date': datetime.now()}
        if password is not None:
            salt = str(random())
            password_hash = hashlib.sha1(bytearray(password + salt, 'utf-8')).hexdigest()
            self.app.db.passwords[user_id] = {'login': user_id, 'salt': salt, 'password': password_hash}

        self.app.db.save_to_disk()
        user.is_registered = True
        return user

    @staticmethod
    def _db_obj_to_user(db_obj):
        if db_obj is None:
            return None
        user_obj = User() if db_obj['is_anonymous'] is not True else AnonymousUser()
        user_obj.name = db_obj['login']
        user_obj.login = db_obj['login']
        user_obj.roles = db_obj['roles']
        user_obj.last_visited_ip = db_obj['ip_address']
        user_obj.is_registered = True
        return user_obj
