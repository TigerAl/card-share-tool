from services.user import UserService
from services.secure import SecureService
from services.game import GameService
from services.deck import DeckService


class Services:
    def __init__(self, app):
        self.user = UserService(app)
        self.secure = SecureService(app)
        self.game = GameService(app)
        self.deck = DeckService(app)
