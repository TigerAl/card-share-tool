from app.webapp import app
from model.game import Game


class GameService:

    def __init__(self, app):
        self.app = app

        # TODO: use DB persistent games register
        self.games = {}
        self.last_game_id = 0

    def get_all_games(self):
        return self.games.values()

    def games_to_dto(self, games_list):
        dto = []
        for game in games_list:
            dto.append(self.game_to_dto(game))
        return dto

    @staticmethod
    def game_to_dto(game):
        dto = {
                'name': game['name'],
                'price': game['price']
            }
        return dto

    def start_new_game(self, deck, owner_user, game_display_name=None):
        game = Game(self._get_next_game_id(), deck, owner_user)
        game.history = []
        game.display_name = game_display_name
        self.games[game.game_id] = game
        return game

    def get_game(self, game_id):
        return self.games.get(game_id)

    def delete_game(self, game_id):
        if game_id in self.games:
            self.games.pop(game_id)
            return True
        return False

    def register_new_player(self, game_id, player):
        game = self.get_game(game_id)
        game.players.push(player)

    def _get_next_game_id(self):
        self.last_game_id += 1
        return str(self.last_game_id)
