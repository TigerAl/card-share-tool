from datetime import datetime
from rsa import PublicKey, PrivateKey


class SecureService:

    def __init__(self, app):
        self.app = app

    @staticmethod
    def key_to_dict(key):
        result = {}
        for attr in key.__slots__:
            result[attr] = str(key[attr])
        return result

    def store_keys(self, public_key, private_key):
        key_storage = self.app.db.secure['key']
        key_storage['id_autoincrement'] += 1
        key_storage[key_storage['id_autoincrement']] = {'private': self.key_to_dict(private_key),
                                                        'public': self.key_to_dict(public_key),
                                                        'created': datetime.now()}
        self.app.db.secure['key'] = key_storage
        self.app.db.secure.sync()
        return key_storage['id_autoincrement']

    def get_keys(self, key_id):
        public_key = private_key = None

        key_storage = self.app.db.secure['key']
        key_obj = key_storage[int(key_id)]
        if key_obj is not None:
            key = key_obj['public']
            public_key = PublicKey(int(key['n']), int(key['e']))
            key = key_obj['private']
            private_key = PrivateKey(int(key['n']), int(key['e']), int(key['d']), int(key['p']), int(key['q']),
                                     int(key['exp1']), int(key['exp2']), int(key['coef']))
        return public_key, private_key
