#! /usr/bin/env python
# -*- coding: utf-8 -*-

from threading import Thread
import os
import sys


def python3_flask_patch():
    global str, unicode, bytes, basestring
    try:
        unicode = unicode
    except NameError:
        # 'unicode' is undefined, must be Python 3
        str = str
        unicode = str
        bytes = bytes
        basestring = (str, bytes)
    else:
        # 'unicode' exists, must be Python 2
        str = str
        unicode = unicode
        bytes = str
        basestring = basestring


def start_web_server():
    python3_flask_patch()

    from app import webapp
    webapp.app.secret_key = 'bla-bla-secret'
    webapp.start_web_app(5000)


def start_gui_app():
    from app import gui
    gui.start_gui_app()


def start_proxy_app():
    from app import proxy
    proxy.start_proxy_app()


class ServerThread(Thread):
    def run(self):
        os._exit(start_web_server())


class GuiThread(Thread):
    def run(self):
        os._exit(start_gui_app())

if __name__ == '__main__':
    if 'server-only' in sys.argv:
        start_web_server()
    elif 'gui-only' in sys.argv:
        start_gui_app()
    elif 'proxy-only' in sys.argv:
        start_proxy_app()
    else:
        GuiThread().start()
        ServerThread().start()

