import shelve


class DB:
    def __init__(self, app):
        self.app = app

        self.common = shelve.open(app.config['DB_COMMON'])
        self.secure = shelve.open(app.config['DB_SECURE'])

        self.users = self.common['user']
        self.passwords = self.secure['password']

    def save_to_disk(self):
        self.common['user'] = self.users
        self.common.sync()
        self.secure['password'] = self.passwords
        self.secure.sync()
