#! /usr/bin/env python

import hashlib
from datetime import datetime
from db.db_local import DB
import config

app = lambda: None
app.config = {}
for key in dir(config):
    if key.isupper():
        app.config[key] = getattr(config, key)

db = DB(app)

# clear database
db.common.clear()
db.secure.clear()


# users
def insert_user(login, password, roles):
    salt = "default_salt"
    password_hash = hashlib.sha1(bytearray(password + salt, 'utf-8')).hexdigest()
    users[login] = {'login': login, 'name': login, 'roles': roles, 'ip_address': None,
                    'is_anonymous': False, 'registration_date': datetime.now()}
    passwords[login] = {'login': login, 'salt': salt, 'password': password_hash}

# initialize DB collections
users = {}
passwords = {}
keys = {'id_autoincrement': 0}

# fill DB with initial data
insert_user('tigeral', 'pass123', ['customer', 'manager', 'admin'])


db.common['user'] = users
db.secure['password'] = passwords
db.secure['key'] = keys

# test products
# db.products.insert({'name': 'product1', 'price': {'value': 20, 'currency': 'UAH'}})
# db.products.insert({'name': 'product2', 'price': {'value': 20, 'currency': 'UAH'}})
# db.products.insert({'name': 'product3', 'price': {'value': 20, 'currency': 'UAH'}})
# db.products.insert({'name': 'product4', 'price': {'value': 20, 'currency': 'UAH'}})

db.common.sync()
db.secure.sync()
db.common.close()
db.secure.close()
