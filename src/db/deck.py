import shelve

class DeckDB:
    def __init__(self, path_to_deck_db_file):
        self.db_file_path = path_to_deck_db_file
        self.db = shelve.open(path_to_deck_db_file)

    def get_name(self):
        return self.db.get('name', None)

    def set_name(self, name):
        self.db['name'] = name

    def get_cards(self):
        return self.db.get('cards', [])

    def get_default_card_back(self):
        return self.db.get('default_back', None)

    def get_images_folder(self):
        return self.db.get('images_folder', None)

    def set_images_folder(self, image_folder):
        self.db['images_folder'] = image_folder

    def get_supported_images_regex(self):
        return self.db.get('supported_images_regex', None)

    def set_supported_images_regex(self, regex_str):
        self.db['supported_images_regex'] = regex_str

