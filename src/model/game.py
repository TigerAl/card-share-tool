class Game:
    def __init__(self, game_id, deck, owner_user):
        self.game_id = game_id
        self.deck = deck
        self.owner = owner_user
        self.players = [owner_user]
        self.history = None
        self.display_name = game_id
