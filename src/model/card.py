class Card:
    def __init__(self, name, groups, display_name, image, back_image, description):
        self.name = name
        self.groups = groups
        self.display_name = display_name
        self.description = description
        self.image = image
        self.back_image = back_image
