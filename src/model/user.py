from enum import Enum
from flask_login import UserMixin


class User(UserMixin):
    roles = []
    locale = None
    theme = None
    login = None
    last_visited_ip = None
    is_registered = False

    def get_id(self):
        return self.login


class AnonymousUser(User):
    @property
    def is_authenticated(self):
        return self.is_registered

    @property
    def is_anonymous(self):
        return True


class Roles(Enum):
    customer = 'CUSTOMER',
    admin = 'ADMIN',
    manager = 'MANAGER'
