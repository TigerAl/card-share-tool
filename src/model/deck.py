class Deck:
    def __init__(self, name):
        self.name = name
        self.folder = ''
        self.default_card_back = ''
        self.cards = {}
        self.images = []
        self.template = '/static/templates/gameboard/default.html'
