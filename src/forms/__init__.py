from wtforms import PasswordField, BooleanField, StringField
from wtforms.meta import DefaultMeta
from wtforms.ext.csrf.fields import CSRFTokenField
from flask_wtf import Form
import rsa
from app.webapp import app


class JsonMeta(DefaultMeta):
    """
    This is the default Meta class which defines all the default values and
    therefore also the 'API' of the class Meta interface.
    """

    def wrap_formdata(self, form, formdata):
        if formdata:
            for field_name in formdata:
                field_data = formdata[field_name]
                if _is_json(field_data) and all(attr in ['data', 'errors'] for attr in field_data):
                    formdata[field_name] = field_data['data']
        return formdata


class DtoForm(Form):

    Meta = JsonMeta

    def to_dto(self):
        form_dto = {}
        for field in self._fields.values():
            if isinstance(field, CSRFTokenField):
                field_data = field.current_token
            else:
                do_not_render = hasattr(field, 'do_not_render') and field.do_not_render is True
                field_data = None if do_not_render else field.data
            field_dto = {'data': field_data if field_data is not None else ''}
            if isinstance(field, RsaEncodedPasswordField) and field.public_key is not None:
                field_dto['public_key'] = field.public_key
            if field.errors is not None and len(field.errors) > 0:
                field_dto['errors'] = field.errors
            form_dto[field.name] = field_dto
        return form_dto


class RsaEncodedPasswordField(PasswordField):
    """
    An RSA encoded StringField. Public key is stored in ...
    """
    data = None
    public_key = None

    def process_data(self, value):
        if value is not None:
            if 'generate_keys' in value and value['generate_keys'] is True:
                (public_key, private_key) = rsa.newkeys(app.config['RSA_COMPLEXITY'], accurate=False)
                key_id = app.services.secure.store_keys(public_key, private_key)
                self.public_key = {'id': str(key_id), 'n': format(public_key.n, 'x'), 'e': format(public_key.e, 'x')}
            if 'do_not_render' in value and value['do_not_render'] is True:
                self.do_not_render = True

    def process_formdata(self, valuelist):
        if valuelist and _is_json(valuelist[0]):
            # form was submitted as JSON object
            field_data = valuelist[0]
            # form is restoring after submission. Supposed that formdata is in JSON format.
            if 'data' in field_data:
                self.data = field_data['data']
            if 'key_id' in field_data:
                self.key_id = field_data['key_id']
                (public_key, private_key) = app.services.secure.get_keys(self.key_id)
                if private_key is not None:
                    self.data = rsa.decrypt(bytearray.fromhex(self.data), private_key).decode('utf-8')
                else:
                    app.logger.warn(gettext("couldn't decode RsaEncodedField because of absent private_key in DB"))
        else:
            # form was submitted as plain HTML form (application/x-www-form-urlencoded)
            self.data = valuelist[0] if valuelist else None


def _is_json(value):
    return value and isinstance(value, dict) and 'data' in value

BooleanField.false_values = (False, 'false', '', None)
