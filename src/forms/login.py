from wtforms import StringField, BooleanField, validators, ValidationError
from app.webapp import app, gettext
from . import RsaEncodedPasswordField, DtoForm


class LoginForm(DtoForm):
    login = StringField(validators=[validators.DataRequired()])
    password = RsaEncodedPasswordField(validators=[validators.DataRequired()])
    remember_me = BooleanField(default=False)

    @staticmethod
    def validate_login(form, field):
        if not app.services.user.is_registered_login:
            raise ValidationError(gettext('current login is not registered in system'))

    @staticmethod
    def validate_password(form, field):
        if len(field.data) < 6:
            raise ValidationError(gettext('password too weak'))
        user_id = app.services.user.find_user_by_login(form.login.data)
        if not app.services.user.match_password(user_id, field.data):
            raise ValidationError(gettext('password does not match to login'))