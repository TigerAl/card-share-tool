from app.webapp import app, flask, render, url_for
from flask import send_file, abort, Response
from api import api_blueprint


@api_blueprint.route('/api/deck')
def get_all_decks():
    decks = app.services.deck.get_all_decks()
    dto = decks_to_dto(decks)
    return flask.jsonify(dto)


@api_blueprint.route('/api/deck/<deck_name>')
def get_deck(deck_name):
    deck = app.services.deck.get_deck(deck_name)
    dto = deck_to_dto(deck)
    return flask.jsonify(dto)


@api_blueprint.route('/api/deck/<deck_name>/images/<image_file_name>')
def get_image(deck_name, image_file_name):
    deck = app.services.deck.get_deck(deck_name)
    if image_file_name in deck.images:
        image_file_path = app.services.deck.get_image_absolute_path(deck_name, image_file_name)
        return send_file(image_file_path)
    abort(404)


@api_blueprint.route('/api/deck/<deck_name>/dynamic.css')
def get_dynamic_css(deck_name):
    deck = app.services.deck.get_deck(deck_name)
    # group cards by image
    card_images_map = {}
    card_back_images_map = {}
    for card in deck.cards:
        card_image_url = url_for('api.get_image', deck_name=deck.name, image_file_name=card.image)
        if card.image not in card_images_map.keys():
            card_images_map[card.image] = {'url': card_image_url, 'cards': [card]}
        else:
            card_images_map[card.image]['cards'].append(card)
        card_back_image = card.back_image or deck.default_card_back
        card_back_image_url = url_for('api.get_image', deck_name=deck.name, image_file_name=card_back_image)
        if card_back_image not in card_back_images_map.keys():
            card_back_images_map[card_back_image] = {'url': card_back_image_url, 'cards': [card]}
        else:
            card_back_images_map[card_back_image]['cards'].append(card)
    return Response(render('gameboard/default.css', deck=deck, card_images_map=card_images_map,
                  card_back_images_map=card_back_images_map), mimetype='text/css')


def decks_to_dto(decks_dict):
    dto = {}
    for deck in decks_dict.values():
        dto[deck.name] = deck_to_dto(deck)
    return dto


def deck_to_dto(deck):
    cards_dto = []
    for card in deck.cards:
        cards_dto.append(card_to_dto(card, deck))
    deck_dto = {
        'id': deck.name,
        'default_card_back': deck.default_card_back or '',
        'cards': cards_dto
    }
    return deck_dto


def card_to_dto(card, deck):
    card_dto = {
        'id': card.name,
        'name': card.display_name,
        'groups': card.groups,
        'image': card.image,
        'back_image': url_for('api.get_image', deck_name=deck.name, image_file_name=card.back_image) if card.back_image is not None else '',
        'description': card.description
    }
    if card.back_image is not None:
        card_dto['back'] = card.back_image
    return card_dto
