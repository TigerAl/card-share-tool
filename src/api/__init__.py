from flask import Blueprint

__all__ = ['login', 'deck', 'api_blueprint']

api_blueprint = Blueprint('api', 'api')
