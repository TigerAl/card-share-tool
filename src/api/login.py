from app.webapp import app, request, flask, make_response, redirect, url_for
from forms.login import LoginForm
from api import api_blueprint


@api_blueprint.route('/api/login', methods=['GET', 'POST'])
def login():
    """
    Collect the default login form data based on the current session information. Also generate a secure token.
    :return: the JSON object with the login, csrf_token, RSA public key for password encoding, and remember_me values.
    """
    if request.method == 'GET':
        form = LoginForm(password={'generate_keys': True})
        return flask.jsonify(form.to_dto())
    elif request.method == 'POST':
        form = LoginForm(password={'do_not_render': True})
        if form.validate_on_submit():
            app.services.user.do_login(form.login.data, form.remember_me.data)
            return make_response()
        else:
            response = flask.jsonify(form.to_dto())
            response.status_code = 400
            return response


@api_blueprint.route("/api/logout", methods=['GET', 'POST'])
def logout():
    app.services.user.do_logout()
    return redirect(request.args.get("next") or url_for("index_page"))
