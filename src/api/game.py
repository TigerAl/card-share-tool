from app.webapp import app, request, flask, current_user
from app.sockets import SocketManager
from api import api_blueprint


@api_blueprint.route('/api/game', methods=['CREATE'])
def create_new_game():
    deck_name = request.args.get('deck')
    if deck_name is None:
        return 'missed query parameter "deck"', 403
    deck = app.services.deck.get_deck(deck_name)
    if deck is None:
        return 'deck "{}" not found'.format(deck_name), 403
    game = app.services.game.start_new_game(deck, current_user)
    return flask.jsonify(game)


@api_blueprint.route('/api/game')
def geta_all_games():
    decks = app.services.deck.get_all_decks()
    return flask.jsonify("")


@api_blueprint.route('/api/game/<game_id>')
def get_game(name):
    deck = app.services.deck.get_deck(name)
    return flask.jsonify("")


@app.sockets.route('/api/game/get_socket')
def get_game_socket(socket):
    while not socket.closed:
        message = socket.receive()
        print(message)
        socket.send('received')
