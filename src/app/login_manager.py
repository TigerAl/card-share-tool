from app.webapp import app, session
from model.user import AnonymousUser
from flask_login import LoginManager as FlaskLoginManager, current_user, login_user as flask_login_user, \
                        logout_user as flask_logout_user, fresh_login_required as flask_fresh_login_required
from functools import wraps


class LoginManager(FlaskLoginManager):
    fresh_login_required = flask_fresh_login_required

    def __init__(self):
        super(LoginManager, self).__init__()
        self.anonymous_users_cache = {}
        self.anonymous_user_counter = 0
        self.user_callback = self._load_user_from_db_or_cache
        self.anonymous_user = self._create_anonymous_user

    def _load_user_from_db_or_cache(self, user_id):
        registered_user = app.services.user.get_user(user_id)
        if registered_user is not None:
            return registered_user
        elif user_id in self.anonymous_users_cache:
            return self.anonymous_users_cache[user_id]
        else:
            return None

    def _create_anonymous_user(self):
        user = AnonymousUser()
        self.anonymous_user_counter += 1
        user_id = 'anonymous-{}'.format(self.anonymous_user_counter)
        user.login = user.name = user_id
        app.login_manager.anonymous_users_cache[user_id] = user
        login_user(user, remember=False)
        return user

    @staticmethod
    def remember_anonymous_user(func):
        @wraps(func)
        def decorated_view(*args, **kwargs):
            if not current_user.is_registered:
                user = current_user._get_current_object()
                app.services.user.register_user(user)
                login_user(user, remember=True)
            return func(*args, **kwargs)
        return decorated_view


LoginManager().init_app(app)


def login_user(user, remember=False, force=False, fresh=True):
    success = flask_login_user(user, remember=remember, force=force, fresh=fresh)
    if success and not remember:
        session['remember'] = 'clear'
    return success


def logout_user():
    app.login_manager.anonymous_users_cache.pop(current_user.get_id(), None)
    return flask_logout_user()

