from flask import render_template


def get_current_theme():
    return 'NO_THEMES_SUPPORTED'


def render(template, **context):
    return render_template(template, **context)
