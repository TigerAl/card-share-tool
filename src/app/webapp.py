import os
import flask
from flask import Flask, render_template, request, session, redirect, g, flash, url_for, jsonify, json, make_response
from flask_babel import Babel, gettext
from db import DB

APP_ROOT_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../..')

# initialize external modules
app = Flask(__name__, instance_path=APP_ROOT_PATH)
app.root_path = APP_ROOT_PATH
app.config.from_object('config')
app.static_folder = 'web/static'
app.template_folder = 'web/templates'
app.themes_folder = 'web/themes'

app.db = DB(app)
app.babel = babel = Babel(app)

# initialize application modules
from app import logging

from app.login_manager import *
from app.themes import *
from views import *
app.register_blueprint(views_blueprint)
from api import *
app.register_blueprint(api_blueprint)
from services import Services
app.services = Services(app)
from app.sockets import SocketManager
app.socket_manager = SocketManager(app)

@babel.localeselector
def get_locale():
    if current_user is not None and hasattr(current_user, 'locale'):
        return current_user.locale
    return request.accept_languages.best_match(app.config['LANGUAGES'].keys())


@babel.timezoneselector
def get_timezone():
    if current_user is not None:
        return current_user.timezone


def start_web_app(port, debug=False):
    # if app.sockets:
    #     from gevent import pywsgi
    #     from geventwebsocket.handler import WebSocketHandler
    #     server = pywsgi.WSGIServer(('', 5000), app, handler_class=WebSocketHandler)
    #     server.serve_forever()
    # else:
    #     app.run(debug=debug, host='0.0.0.0', port=port)
    app.run(debug=debug, host='0.0.0.0', port=port)
