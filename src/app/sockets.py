import asyncore
import socket
from app.webapp import app


class SocketManager:

    def __init__(self, app, address='0.0.0.0', ports_range=None):
        """

        :param app: link to app object
        :param address: default value is '0.0.0.0'
        :param ports_range: use range(5001, 5100) if you want specify a ports range or None if you want get port from OS
        """
        self.app = app
        self.address = address
        self.ports_range = ports_range

        self.sockets_to_game_mapping = {}
        self.sockets_map = {}
        self.sockets_to_port_map = {}
        self.__socket_id = 0

    def create_game_socket(self, game, port=None):
        if port is None:
            if self.ports_range is None:
                port = 0
            else:
                for port in self.ports_range:
                    if port not in self.sockets_to_port_map:
                        break
                else:
                    app.logger
        socket_server = SocketServer(('localhost', port))
        socket_client = SocketClient
        self.__socket_id += 1
        socket_id = self.__socket_id
        socket_obj = {'id': socket_id, 'server': socket_server, 'client': socket_client, 'game': game}

        self.sockets_map[socket_id] = socket_obj
        self.sockets_to_game_mapping[game.game_id] = socket_obj
        self.sockets_to_port_map[port] = socket_obj
        return socket_obj

    def close_game_socket(self, socket_id):
        socket_obj = self.sockets_map.pop(socket_id)
        self.sockets_map.pop(socket_id)
        self.sockets_to_game_mapping.pop(socket_obj.game)
        self.sockets_to_port_map.pop(socket_obj.server.port)
        socket_obj.server.handle_close()
        if socket_obj.server.handler is not None:
            socket_obj.server.handler.handle_close()
        socket_obj.client.handle_close()


class SocketServer(asyncore.dispatcher):
    """Receives connections and establishes handlers for each client.
    """

    def __init__(self, address):
        self.logger = logging.getLogger('EchoServer')
        asyncore.dispatcher.__init__(self)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.bind(address)
        self.address = self.socket.getsockname()
        self.logger.debug('binding to %s', self.address)
        self.listen(1)
        self.port = address[1]
        self.handler = None

    def handle_accept(self):
        client_info = self.accept()
        self.logger.debug('handle_accept() -> %s', client_info[1])
        self.handler = SocketHandler(sock=client_info[0])
        self.handle_close()
        return

    def handle_close(self):
        self.logger.debug('handle_close()')
        self.close()
        return

class SocketHandler(asyncore.dispatcher):
    """Handles echoing messages from a single client.
    """

    def __init__(self, sock, chunk_size=256):
        self.chunk_size = chunk_size
        self.logger = logging.getLogger('EchoHandler%s' % str(sock.getsockname()))
        asyncore.dispatcher.__init__(self, sock=sock)
        self.data_to_write = []
        return

    def writable(self):
        """We want to write if we have received data."""
        response = bool(self.data_to_write)
        self.logger.debug('writable() -> %s', response)
        return response

    def handle_write(self):
        """Write as much as possible of the most recent message we have received."""
        data = self.data_to_write.pop()
        sent = self.send(data[:self.chunk_size])
        if sent < len(data):
            remaining = data[sent:]
            self.data.to_write.append(remaining)
        self.logger.debug('handle_write() -> (%d) "%s"', sent, data[:sent])
        if not self.writable():
            self.handle_close()

    def handle_read(self):
        """Read an incoming message from the client and put it into our outgoing queue."""
        data = self.recv(self.chunk_size)
        self.logger.debug('handle_read() -> (%d) "%s"', len(data), data)
        self.data_to_write.insert(0, data)

    def handle_close(self):
        self.logger.debug('handle_close()')
        self.close()


class SocketClient(asyncore.dispatcher):
    """Sends messages to the server and receives responses.
    """

    def __init__(self, host, port, message, chunk_size=512):
        self.message = message
        self.to_send = message
        self.received_data = []
        self.chunk_size = chunk_size
        self.logger = logging.getLogger('EchoClient')
        asyncore.dispatcher.__init__(self)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.logger.debug('connecting to %s', (host, port))
        self.connect((host, port))
        return

    def handle_connect(self):
        self.logger.debug('handle_connect()')

    def handle_close(self):
        self.logger.debug('handle_close()')
        self.close()
        received_message = ''.join(str(self.received_data))
        if received_message == self.message:
            self.logger.debug('RECEIVED COPY OF MESSAGE')
        else:
            self.logger.debug('ERROR IN TRANSMISSION')
            self.logger.debug('EXPECTED "%s"', self.message)
            self.logger.debug('RECEIVED "%s"', received_message)
        return

    def writable(self):
        self.logger.debug('writable() -> %s', bool(self.to_send))
        return bool(self.to_send)

    def handle_write(self):
        sent = self.send(self.to_send[:self.chunk_size])
        self.logger.debug('handle_write() -> (%d) "%s"', sent, self.to_send[:sent])
        self.to_send = self.to_send[sent:]

    def handle_read(self):
        data = self.recv(self.chunk_size)
        self.logger.debug('handle_read() -> (%d) "%s"', len(data), data)
        self.received_data.append(data)
