#!/usr/bin/python3

import sys
from PyQt5.QtCore import *
from PyQt5.QtWebKitWidgets import QWebView
from PyQt5.QtWidgets import QApplication

def start_gui_app():
    app = QApplication(sys.argv)
    web = QWebView()
    web.load(QUrl("http://localhost:5000"))
    web.show()
    return app.exec_()

if __name__ == '__main__':
    sys.exit(start_gui_app())
