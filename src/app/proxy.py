import os
import sys
import shelve
from flask import Flask
from flask import request, Response
from flask import stream_with_context

import requests

APP_ROOT_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../..')
proxy = Flask(__name__, instance_path=APP_ROOT_PATH)
proxy.config.from_object('config')
proxy.has_static_folder = False

proxy.app_id_autoincrement = 0
proxy.db = shelve.open(proxy.config['PROXY_DB'])
proxy.last_active_app_id = proxy.db['last_active_app_id'] if 'last_active_app_id' in proxy.db else None
remote_apps = proxy.db['remote_apps'] if 'remote_apps' in proxy.db else {}


@proxy.route('/proxy/register_app')
def register_app():
    """
    Register new application server with specified IP address and Port.
    If the application server with same IP and Port is already registered then return its app_id.

    :return: app_id of the registered application server.
    """
    # validate request params
    ip = request.args.get('ip')
    port = request.args.get('port')
    if ip is None or port is None:
        missed_params = list(filter(lambda param: request.args.get(param) is None, ['ip', 'port']))
        return 'The query params "{}" is missed.'.format('", "'.join(missed_params)), 403

    # check if this application server is already registered
    for app_id, remote_app in remote_apps.items():
        if remote_app['ip'] == ip and remote_app['port'] == port:
            break
    else:
        # register new app server
        proxy.app_id_autoincrement += 1
        app_id = str(proxy.app_id_autoincrement)
        remote_apps[app_id] = {'ip': ip, 'port': port}
        proxy_db['remote_apps'] = remote_apps
        proxy.last_active_app_id = app_id
        proxy_db['last_active_app_id'] = proxy.last_active_app_id
        proxy_db.sync()
    return Response(str(app_id), 200)


@proxy.route('/proxy/register_socket')
def register_socket():
    return Response(str(0), 200)


@proxy.route('/<path:url>')
@proxy.route('/')
def do_proxy(url=""):
    app_id = request.cookies['ProxyAppId'] if 'ProxyAppId' in request.cookies else None
    if app_id not in remote_apps:
        if proxy.config['PROXY_AUTOCONNECT_CLIENT_TO_LAST_ACTIVE_SERVER'] and proxy.last_active_app_id is not None:
            app_id = proxy.last_active_app_id
        else:
            return 'Remote app server with ID = {} is not found.'.format(app_id), 404
    remote_app = remote_apps[app_id]
    req = requests.get('http://{}:{}/{}'.format(remote_app['ip'], remote_app['port'], url), stream=True)
    return Response(stream_with_context(req.iter_content()), content_type=req.headers['content-type'])


def start_proxy_app():
    proxy.run(host='0.0.0.0', port=proxy.config['PROXY_PORT'], debug=False)

    print('proxy app started')


if __name__ == '__main__':
    sys.exit(start_proxy_app())
