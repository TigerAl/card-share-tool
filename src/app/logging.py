from app.webapp import app
import sys
import logging
from logging.handlers import SMTPHandler, TimedRotatingFileHandler


if app.config['ERROR_EMAILS_ENABLE']:
    mail_handler = SMTPHandler(mailhost=('smtp.gmail.com', 587),
                               fromaddr='admin@tigeral.pp.ua',
                               toaddrs=['admin@tigeral.pp.ua'],
                               subject='card-share-tool error mail',
                               credentials=('admin@tigeral.pp.ua', 'syhwbuktsoczsmix'),
                               secure={})
    mail_handler.setLevel(logging.ERROR)
    app.logger.addHandler(mail_handler)

if app.config['FILE_LOG_ENABLE']:
    file_handler = TimedRotatingFileHandler(filename='logs/console',
                                            when='MIDNIGHT',
                                            interval=1,
                                            backupCount=30)
    file_handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    file_handler.setFormatter(formatter)
    app.logger.addHandler(file_handler)

if app.config['ERROR_FILE_LOG_ENABLE']:
    file_handler = TimedRotatingFileHandler(filename='logs/errors',
                                            when='MIDNIGHT',
                                            interval=1,
                                            backupCount=30)
    file_handler.setLevel(logging.ERROR)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    file_handler.setFormatter(formatter)
    app.logger.addHandler(file_handler)

if app.config['CONSOLE_LOG_ENABLE']:
    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    console_handler.setFormatter(formatter)
    app.logger.addHandler(console_handler)