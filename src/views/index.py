from app.webapp import render
from views import views_blueprint


@views_blueprint.route('/')
def index_page():
    # return redirect(url_for('game_manager'))
    return render('index.html')
