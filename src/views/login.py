from app.webapp import app, render, redirect, request, url_for
from forms.login import LoginForm
from views import views_blueprint


@views_blueprint.route('/login', methods=['GET'])
def login_page():
    return render('login_page.html')


@views_blueprint.route('/login', methods=['GET'])
def register_page():
    return render('login_page.html')


@views_blueprint.route('/login', methods=['POST'])
def login():
    return render('login_page.html')


@views_blueprint.route('/form/login', methods=['GET', 'POST'])
def login_form():
    form = LoginForm()
    return render('login_form.html', form=form, submit_url=url_for('api.login'))


@views_blueprint.route("/logout", methods=['GET', 'POST'])
def logout():
    app.services.user.do_logout()
    return redirect(request.args.get("next") or url_for("views.index_page"))
