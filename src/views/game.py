from app.webapp import app, current_user, redirect, url_for, render
from flask import request
from views import views_blueprint
from app.login_manager import LoginManager


@views_blueprint.route('/new_game', methods=['GET'])
def start_new_game():
    deck_name = request.args.get('deck')
    if deck_name is None:
        return 'missed query parameter "deck"', 403
    deck = app.services.deck.get_deck(deck_name)
    if deck is None:
        return 'deck "{}" not found'.format(deck_name), 403
    game = app.services.game.start_new_game(deck, current_user)
    return redirect(url_for('views.game_page', game_id=game.game_id))


@views_blueprint.route('/game/<game_id>')
@LoginManager.remember_anonymous_user
def game_page(game_id):
    game = app.services.game.get_game(game_id)
    if game is None:
        return 'game with id="{}" not found'.format(game_id), 404
    return render('gameboard/default.html', game=game)
