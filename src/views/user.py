from app.webapp import render
from views import views_blueprint
from app.login_manager import LoginManager


@views_blueprint.route("/profile")
def profile_page():
    return render('user_profile.html')


@views_blueprint.route("/profile/password_change")
@LoginManager.fresh_login_required
def password_change():
    pass
