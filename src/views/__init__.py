from flask import Blueprint

__all__ = ['index', 'login', 'user', 'game', 'games_manager', 'views_blueprint']


views_blueprint = Blueprint('views', 'views')
