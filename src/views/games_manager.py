from app.webapp import app, render
from views import views_blueprint


@views_blueprint.route('/games')
def games_manager():
    games_list = app.services.game.get_all_games()
    return render('games_manager.html', games_list=games_list)

