# -*- coding: utf-8 -*-

DB_COMMON = './data/common.db'
DB_SECURE = './data/secure.db'
DECK_DATA_FOLDER = './data/deck'
DECK_SUPPORTED_IMAGES_REGEX = '.*\\.(jpg|svg)$'
DECK_DB_FILE_NAME = 'config.db'
DECK_HISTORY_FILE_NAME = 'history.db'
DECK_IMAGES_FOLDER = 'images'

# -----------  localization section  -------------
BABEL_DEFAULT_LOCALE = 'en'
BABEL_DEFAULT_TIMEZONE = 'UTC'
LANGUAGES = {
    'en': 'English',
    'ua': 'Українська',
    'ru': 'Русский'
}

# ------------------  theme managment  -----------
DEFAULT_THEME = 'default'

# -------------  security section  ---------------
# RSA encoding used to encrypt some sensitive data which are sending over HTTP (if you won't use HTTPS in some reason).
# RSA keys pare is a one-time used keys and bounds with particular WTF form (WTF_CSRF security token is used as a key
# to store an RSA decoding key in DB).
RSA_COMPLEXITY = 512
RSA_ENCODING_PASSWORD_ENABLED = True
RSA_KEY_EXPIRATION_TIME = 3600  # how long the RSA keys will be kept in DB. All expired keys will be erased by cronjob.
WTF_CSRF_ENABLED = True

# -------------  user management  ---------------
AUTOLOGIN_ANONYMOUS_USER = True
REGISTER_ANONYMOUS_USER = True
SESSION_PROTECTION = 'basic'  # This can be either 'basic' (the default) or 'strong', or None to disable it.

# -----------------  logging (see app/loging.py for details)   --------------------
CONSOLE_LOG_ENABLE = True  # logging to the stdout stream.
FILE_LOG_ENABLE = True  # logging to the logs/console file. Each midnight old log become to logs/console.YYYY-mm-dd file
ERROR_FILE_LOG_ENABLE = True  # logging to the logs/errors file. Same as FILE_LOG_ENABLE.
ERROR_EMAILS_ENABLE = False  # send an emails if any ERROR level log message appears.

# -----------------  proxy configuration  ---------------------
PROXY_PORT = 8000
PROXY_HOST = 'tigeral.pp.ua'
PROXY_DB = './data/proxy.db'
PROXY_AUTOCONNECT_CLIENT_TO_LAST_ACTIVE_SERVER = True
