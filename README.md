# README #

This application is under construction. It is completely not usable yet.

This README page is created only to keep some legal information.

# Legal info #

1. This project isn't ready for any kind of distribution but **you could use it for information purpose only**. Later it would be released under some free license (probably BDS or MPL), but right now I didn't chose the license model yet.

2. Right now, this code contains some vector graphics released under GPL3 license. You could find these images in data/deck/default/images folder. Here is an author copyright info:
> Vectorized Playing Cards 2.0 - http://sourceforge.net/projects/vector-cards/
>
> Copyright 2015 - Chris Aguilar - conjurenation@gmail.com
>
> Licensed under LGPL 3 - www.gnu.org/copyleft/lesser.html