
angular.module('gamePage', ['ui.bootstrap'])
.controller('gameController', ['$scope', '$http', '$sce', function(scope, http, sce) {
  scope.game = new Game();
  scope.gameboard = new GameBoard(scope.game, scope);

  scope.dragZIndexYou = 101;
  scope.dragZIndexOtherPlayers = 100;
  scope.dragMinTimeInterval = 100;
  scope.dragClickMaxDelay = 600;
  scope.dragClickMaxMoveDistance = 20;
  scope.dropSlotsList = [];

  scope.init = function(deckName) {
    // request game data
    http.get('/api/deck/' + deckName).then(function successCallback(response) {
      // console.log(response);
      scope.deck = response.data;
      for (var i in scope.deck.cards) {
        var cardDto = scope.deck.cards[i];
        var cardBackImage = cardDto.back_image || scope.deck.default_card_back;
        var card = new Card(cardDto.id, cardDto.name, cardDto.type, cardDto.id, cardBackImage, cardDto.description);
        scope.gameboard.addCard(card, scope.gameboard.getSlot('deck_slot'));

        // TODO: remove debug line below
        if (i == 5) break;
      }

      var player = new Player('game_owner');
      scope.game.onGameStart(scope.deck, player);
    }, function errorCallback(response) {
      showError('Can\'t load deck data.');
    });

    window.gameScope = scope;
  };

  scope.getCardByElement = function (cardElement){
    return angular.element(cardElement).scope().card;
  };

  scope.showError = function (msg) {
    console.error(msg);
  };

  scope.findCardSlotElement = function(element) {
    if (element[0].isCardSlot) {
      return element;
    } else if (element[0] == document.body) {
      return null;
    } else {
      return scope.findCardSlotElement(element.parent());
    }
  };
}])
.directive('card', function() {
  return {
    scope: {
      model: '=?ngModel',
    },
    transclude: {
      'cardInner': '?cardInner'
    },
    template:'<div class="card-inner-div" ' +
             'ng-class="{opened: model.isOpened, closed: !model.isOpened, \'glow-small-green\': model.isSelected}" ' +
             'ng-transclude="cardInner"' +
             'uib-popover-template="\'cardActionButtonsPopover.html\'" ' +
             'popover-placement="bottom" popover-is-open="model.actionButtonsShown"' +
             'popover-trigger="none">' +
             '</div>',
    controller: 'gameController',
    link: function(scope, element, attrs, ctrl) {
      // assign parent scope variables
      scope.game = scope.$parent.game;
      scope.gameboard = scope.$parent.gameboard;

      // initialize DOM element properties
      element.attr('id', 'card-' + scope.model.id);
      element.addClass('card');
      // set card position in slot
      var card = scope.model;
      var cardSlot = scope.$parent.model;
      if (!cardSlot.arrangeCardPos && card.position) {
        element.offset({left: card.position.left + cardSlot.element.offset().left,
                        top: card.position.top + cardSlot.element.offset().top});
      }

      // apply default values to scope variables
      //TODO

      // drag & drop
      element.draggable({
        start: function (event, ui) {
          // remember card position on drag start and init drag&drop specific properties
          card.moveToInitialPosOnDragStop = false;
          card.clearPositionOnDragStop = false;
          card.position = ui.offset;
          card.dragInitialPosition = Object.assign({}, card.position);
          card.element = element;
          card.dragStartTimestamp = Date.now();

          // dom elements modification
          ui.helper.css('z-index', scope.dragZIndexYou);

          // process event on game level
          scope.gameboard.onCardDragStart(card);
          scope.$apply();
        },
        stop: function (event, ui) {
          // process drop events (could contain multiple drop events if droppable elements overlaps)
          // TODO: think about better solution to solve jQuery droppable z-index problem
          for (var dropSlotIndex in gameScope.dropSlotsList) {
            var dropSlot = gameScope.dropSlotsList[dropSlotIndex];
            if (gameScope.dropSlotsList.length > 1 && dropSlot.isGlobalCardSlot) {
              continue;
            }
            // try to place card to slot
            if (dropSlot.cardsLimit && dropSlot.cards.length >= dropSlot.cardsLimit) {
              gameScope.gameboard.onDropCardToFullSlot(card, dropSlot);
            } else {
              gameScope.gameboard.onDropCardToSlot(card, dropSlot, {
                left: ui.offset.left - dropSlot.element.offset().left,
                top: ui.offset.top - dropSlot.element.offset().top
              });
            }
          }
          gameScope.dropSlotsList = [];

          // process card position on drag end and remove drag&drop properties
          if (card.moveToInitialPosOnDragStop) {
            ui.helper.offset(card.dragInitialPosition);
          }
          if (card.clearPositionOnDragStop) {
            ui.helper.css({left: '', top: ''});
          }
          delete card.moveToInitialPosOnDragStop;
          delete card.clearPositionOnDragStop;
          delete card.dragInitialPosition;
          delete card.dragStartTimestamp;

          // dom elements modification
          ui.helper.css('z-index', '');

          // process event on game level
          scope.gameboard.onCardDragStop(card);
          scope.$apply();
        },
        drag: function (event, ui) {
          // update card position because it is not bind by angular
          card.position = ui.offset;
          scope.gameboard.onCardDragMove(scope.model);
        }
      });
      
      element.click(function (event) {
        // ignore click if it was initiated by popover element
        if (event.target != this) {
          for (var i in event.originalEvent.path) {
            var pathElement = event.originalEvent.path[i];
            if (pathElement.className.startsWith('popover')) {
              return true;
            } else if (pathElement == this) {
              break;
            }
          }
        }

        scope.gameboard.onCardClick(scope.model);
        scope.$apply();
      });


    }
  }
})
.directive('cardslot', function() {
  return {
    scope: {
      model: '=?ngModel',
      cardsToShow: '=?',
      cardPosShift: '=?',
      cardsLimit: '=?',
      globalCardSlot: '=?',
      highlightOnDragStart: '=?',
      highlightOnDragOver: '=?',
      arrangeCardPos: '=?'
    },
    transclude: {
      'beforeCards': '?beforeCards',
      'cardInner': '?card-inner'
    },
    template: '<div style="position: relative; left: 0px; top: 0px;" ng-transclude="beforeCards"></div>' +
              '<card ng-repeat="card in model.cards" ng-model="card" ' +
                    'ng-hide="(arrangeCardPos && $index >= cardsToShow)" ' +
                    'ng-class="(arrangeCardPos && $index < cardsToShow) ? \'card-stack-{{$index}}\' : \'\'">' +
              '  <card-inner ng-transclude="cardInner"></card-inner>' +
              '</card>' +
              '<div style="position: absolute; top: -20px">{{model.cards.length}}</div>',
    controller: 'gameController',
    link: function(scope, element, attrs, ctrl) {
      // assign parent scope variables
      scope.game = scope.$parent.game;
      scope.gameboard = scope.$parent.gameboard;

      // initialize card slot in gameboard
      if (attrs.id === undefined) {
        // look for free slot index
        var cardSlotIndexes = [];
        $('cardslot').each(function (slotIndex, cardSlot) {
          var match_result = /^slot-(\d+)$/.exec(cardSlot.id);
          if (match_result != null && match_result.length == 2) {
            cardSlotIndexes.push(parseInt(match_result[1]));
          }
        });
        for (var slotIndex = 0; slotIndex < cardSlotIndexes.length; slotIndex++) {
          if (cardSlotIndexes.indexOf(slotIndex) < 0) {
            break;
          }
        }
        attrs.id = 'slot-' + slotIndex;
      }
      var cardSlot = scope.gameboard.getSlot(attrs.id);
      if (cardSlot == null) {
        cardSlot = new CardSlot(attrs.id);
        scope.gameboard.slots.push(cardSlot);
      }

      // initialize DOM element properties
      element.attr('id', attrs.id);
      element.addClass('card-slot');
      element[0].isCardSlot = true;

      // apply default values to scope variables
      cardSlot = scope.model = scope.model || cardSlot;
      scope.cardsToShow = scope.cardsToShow || 3;
      scope.cardPosShift = scope.cardPosShift || 2;
      cardSlot.cardsLimit = scope.cardsLimit = scope.cardsLimit || null;
      scope.highlightOnDragStart = scope.highlightOnDragStart || false;
      scope.highlightOnDragOver = scope.highlightOnDragOver || false;
      cardSlot.arrangeCardPos = scope.arrangeCardPos = scope.arrangeCardPos || false;
      scope.greedy = scope.greedy === undefined ? true : scope.greedy;
      cardSlot.element = element;

      // detect global card slot
      if (scope.globalCardSlot) {
        cardSlot.isGlobalCardSlot = scope.globalCardSlot;
      }

      // drag & drop
      element.droppable({
        drop: function(event, ui) {
          //check if card was clicked with a small position shift (that should not be handled as drag&drop)
          var card = scope.getCardByElement(ui.draggable);
          var dragDurationMillis = Date.now() - card.dragStartTimestamp;
          if (dragDurationMillis < gameScope.dragMinTimeInterval
            || (dragDurationMillis < gameScope.dragClickMaxDelay
              && (Math.abs(ui.offset.left - card.dragInitialPosition.left) < gameScope.dragClickMaxMoveDistance
                && Math.abs(ui.offset.top - card.dragInitialPosition.top) < gameScope.dragClickMaxMoveDistance)))
          {
            gameScope.gameboard.onCardClick(card);
          }

          // just collect drop event parameters. The event itself will be processed in draggable.stop handler.
          gameScope.dropSlotsList.push(cardSlot);
        },
        activate: function() {
          if (scope.highlightOnDragStart) {
            if (scope.model.cards.length < scope.cardsLimit) {
              element.addClass('highlight-accept-more');
            } else {
              element.addClass('highlight-cannot-accept');
            }
          }
        },
        over: function() {
          if (scope.highlightOnDragOver) {
            if (scope.model.cards.length < scope.cardsLimit) {
              element.addClass('highlight-accept-more');
            } else {
              element.addClass('highlight-cannot-accept');
            }
          }
        },
        deactivate: function() {
          if (scope.highlightOnDragStart) {
            element.removeClass('highlight-accept-more highlight-cannot-accept');
          }
        },
        out: function() {
          if (scope.highlightOnDragOver) {
            element.removeClass('highlight-accept-more highlight-cannot-accept');
          }
        }
      });


    }
  }
});
