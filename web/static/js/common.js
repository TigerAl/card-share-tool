function getUrlParameter(parameterName) {
  var queryParams = window.location.search.substring(1).split('&');
  for (var queryParameter in queryParams) {
    var keyValuePair = queryParameter.split('=');
    if (keyValuePair[0] == parameterName) {
      return keyValuePair[1];
    }
  }
}

function touchHandler(event) {
  var touch = event.changedTouches[0];

  var simulatedEvent = document.createEvent("MouseEvent");
  simulatedEvent.initMouseEvent({
        touchstart: "mousedown",
        touchmove: "mousemove",
        touchend: "mouseup"
      }[event.type], true, true, window, 1,
      touch.screenX, touch.screenY,
      touch.clientX, touch.clientY, false,
      false, false, false, 0, null);

  touch.target.dispatchEvent(simulatedEvent);
  event.preventDefault();
}

function initTouchHandler() {
  document.addEventListener("touchstart", touchHandler, true);
  document.addEventListener("touchmove", touchHandler, true);
  document.addEventListener("touchend", touchHandler, true);
  document.addEventListener("touchcancel", touchHandler, true);
}

$(function () {
  initTouchHandler();
});


/**
 * If multiple arguments then log will be grouped with the first argument as group label.
 * @param {...*} args
 */
function log(args) {
  if (arguments.length > 1) {
    console.groupCollapsed(arguments[0]);
    for (var i=1; i < arguments.length; i++) {
      console.log(arguments[i]);
    }
    console.groupEnd();
  }
  else if (arguments.length == 1) {
    console.log(arguments[0]);
  }
}

function debug(args) {
  if (arguments.length > 1) {
    console.groupCollapsed(arguments[0]);
    for (var i=1; i < arguments.length; i++) {
      console.trace(arguments[i]);
    }
    console.groupEnd();
  }
  else if (arguments.length == 1) {
    console.trace(arguments[0]);
  }
}