function Card (id, name, type, image, backImage, description) {
  this.id = id;
  this.name = name;
  this.type = type;
  this.image = image;
  this.backImage = backImage;
  this.description = description;
  this.selectedBy = []; // list of user IDs
  this.isOpened = false;
  this.isSelected = false;
  this.isArrangedInSlot = false;
  this.position = {left: 0, top: 0};
}

function CardSlot (id) {
  this.id = id;
  this.cards = [];
  this.selectedBy = []; // list of user IDs
  this.limit = null;
  this.arrangeCardPos = false;
}

// UI representation of game board. Any game logic actions should be done using Game class.
function GameBoard (game, game_page_controller_scope) {
  this.slots = [];
  this.cards_mapping = {};  // this is a map where key is a card and value is a card slot
  this.game = game;
  game.gameboard = this;
  this.game_page_controller_scope = game_page_controller_scope;

  /* methods to update UI model (an UI itself will be updated by Angular and game_page.js script) */
  this.getSlot = function (slotId) {
    for (var i in this.slots) {
      var slot = this.slots[i];
      if (slot.id == slotId) {
        return slot;
      }
    }
    return null;
  };
  this.addCard = function (card, slot) {
    for (var storedCardId in this.cards_mapping) {
      if (storedCardId == card.id) {
        debug('gameboard.addCard: card with the same id already exist.', card, storedCardId);
        return;
      }
    }
    if (slot) {
      slot.cards.push(card);
      this.cards_mapping[card.id] = slot;
    }
    else {
      this.cards_mapping[card.id] = null;
    }
  };
  this.removeCardFromSlot = function (card, slot) {
    slot.cards.splice(slot.cards.indexOf(card), 1);
    this.cards_mapping[card.id] = null;
  };
  this.moveCardToSlot = function (card, slot, pos) {
    var cardSlot = this.cards_mapping[card.id];
    if (cardSlot != null) {
      cardSlot.cards.splice(cardSlot.cards.indexOf(card), 1);
    }
    slot.cards.push(card);
    this.cards_mapping[card.id] = slot;
    if (slot.arrangeCardPos) {
      this.resetCardPosition(card);
    } else {
      card.position = pos;
    }
  };
  this.resetCardPosition = function (card) {
    var slot = this.cards_mapping[card.id];
    if (slot && slot.arrangeCardPos) {
      card.clearPositionOnDragStop = true;
    }
    else {
      card.moveToInitialPosOnDragStop = true;
    }
  };

  /* methods to handle UI events */
  this.onCardDragStart = function (card) {
    log('card drag start', card);
  };
  this.onCardDragStop = function (card) {
    log('card drag stop', card);
  };
  this.onCardDragMove = function (card) {
    // log('card drag move', card);
  };
  this.onCardClick = function (card) {
    log('card clicked', card);
    card.actionButtonsShown = !card.actionButtonsShown;
  };
  this.onCardDblClick = function (card) {
    log('card double clicked', card);
  };
  this.onDropCardToSlot = function (card, slot, pos) {
    log('card dropped to slot', card, slot, pos);
    game.moveCardToSlot(card, slot, pos)
  };  
  this.onDropCardToFullSlot = function (card, slot) {
    log('card dropped to full slot', card, slot);
    this.resetCardPosition(card);
  };
  this.onCardSlotClick = function (card, slot) {
    log('card dropped to slot', card, slot);
  };
  this.onCardOpenButtonClick = function (card) {
    log(card.isOpened ? 'card opened' : 'card closed', card);
    card.isOpened = !card.isOpened;
    card.actionButtonsShown = !card.actionButtonsShown;
  };
  this.onCardDetailsButtonClick = function (card) {
    log('card details button clicked', card);
  };
  this.onCardSelectButtonClick = function (card) {
    if (card.isSelected) {
      log('card selected', card);
      this.game.clearSelection(this.game.player, card);
    } else {
      log('card selection removed', card);
      this.game.selectCard(this.game.player, card);
    }
  };
  this.onCardPopoverCloseButtonClick = function (card) {
    log('close card action buttons popover', card);
    card.actionButtonsShown = false;
  };
}

function Game (gameboard) {
  this.players = {};
  this.owner = "";
  this.player = "";
  this.gameboard = null;
  this.selection = {};

  this.onGameStart = function (deck, player) {
    log('game started', deck, this, this.gameboard);
    this.addPlayer(player);
    this.player = player;
    this.owner = player;
  };

  this.moveCardToSlot = function (card, slot, pos, doSendEvent) {
    if (doSendEvent) {
      /* send event to server */
    }
    this.gameboard.moveCardToSlot(card, slot, pos);
  };
  
  this.addPlayer = function (player) {
    this.players[player.name] = player;
    this.selection[player] = [];
  };
  
  this.selectCard = function (player, card) {
    card.isSelected = true;
    this.selection[player].push(card);
  };

  this.clearSelection = function (player, cardOrSlot) {
    if (player) {
      for (var i in this.selection[player]) {
        var selectedObj = this.selection[player][i];
        if (cardOrSlot) {
          if (cardOrSlot == selectedObj) {
            selectedObj.isSelected = false;
            this.selection[player].splice(i, 1);
            return;
          }
        } else {
            selectedObj.isSelected = false;
        }
      }
      this.selection[player] = [];
    } else {
      for (player in this.selection) {
        this.clearSelection(player);
      }
    }
  };
}

function Player (name) {
  this.name = name;
}
