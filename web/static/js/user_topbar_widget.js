
angular.module('userTopbarWidget', ['ui.bootstrap'])
.controller('userController', ['$scope', '$http', function(scope, http) {

  scope.popupVisible = false;
  scope.init = function(element) {
    console.log('initialized');
  };
}])
.directive('userTopbarWidget', function() {
  return {
    controller: 'userController',
    link: function(scope, element, attrs, ctrl) {
      element.click(function (event) {
        scope.popupVisible = !scope.popupVisible;
        event.stopPropagation();
        scope.$apply();
      });
    }
  }
})
.directive('userTopbarWidgetPopup', function() {
  return {
    controller: 'userController',
    link: function(scope, element, attrs, ctrl) {
      element.click(function (event) {
        event.stopPropagation();
        scope.$apply();
      });
      $(document).click(function () {
        scope.popupVisible = false;
        scope.$apply();
      });
    }
  }
});

var userTopbarWidget = $("#user-topbar-widget");
angular.element(userTopbarWidget).ready(function() {
  angular.bootstrap(userTopbarWidget, ["userTopbarWidget"]);
});

