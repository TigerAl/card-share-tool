angular.module('loginPage', ['ui.bootstrap']).controller('loginFormController', ['$scope', '$http', function($scope, $http) {

    $scope.loginForm = {};
    $scope.loginForm.password = {};

    $scope.init = function() {
        $http.get('/api/login')
            .success(function (data, status, headers, config) {
                // keep a customer data prefilled by browser itself
                if ($scope.loginForm.login && $scope.loginForm.login.data && !data.login.data) {
                    data.login.data = $scope.loginForm.login.data;
                }
                // prefill data from the REST api
                $scope.loginForm.login = data.login;
                // avoid to reset a password.data value because it couldn't be operate till form submit
                $scope.loginForm.password.public_key = data.password.public_key;
                $scope.loginForm.password.errors = data.password.errors;
                $scope.loginForm.remember_me = data.remember_me;
                $scope.loginForm.csrf_token = data.csrf_token;
            })
            .error(function (data, status, headers, config) {
                alert('form data initialization failed. status = ' + status + ', data = ' + data);
            });
    };

    $scope.submit = function() {
        var form = $scope.loginForm;
        var submitForm = {'login': form.login,
                          'password': form.password,
                          'remember_me': form.remember_me,
                          'csrf_token': form.csrf_token};
        if (form.password.public_key) {
            var rsa = new RSAKey();
            rsa.setPublic(form.password.public_key.n, form.password.public_key.e);
            var encodedPassword = rsa.encrypt(form.password.data);
            if (encodedPassword) {
                submitForm.password = { 'data': encodedPassword, 'key_id': form.password.public_key.id }
            }
            else {
                form.error = "something wrong with password encription";
                console.error(form.error);
            }
        }

        $http.post('/api/login', submitForm)
            .success(function(data, status, headers, config) {
                var next = getUrlParameter('next');
                location = next ? next : '/profile';
            })
            .error(function(data, status, headers, config) {
                if (data) {
                    $scope.loginForm.login.errors = data.login ? data.login.errors : [];
                    $scope.loginForm.password.errors = data.password ? data.password.errors : [];
                    $scope.loginForm.remember_me.errors = data.remember_me ? data.remember_me.errors : [];
                    // $scope.$apply();
                }
                else {
                    alert('form submit failed. status = ' + status +', data = ' + data);
                }
            });
    };
}]);
